# python_module

This is a python api to access the mednicklab database. 

## Installation:

First install python on your machine. The best way to do this is using [Anaconda](https://www.anaconda.com/distribution/).
Next, simply pip install this repository:  
```
pip install git+https://bitbucket.org/mednicklab/mednickdb_pyapi.git
```

The examples folder has some simple examples to help you get started. Usecase.ipynb is a good place to begin.
You can run this notebook with  
```
cd examples/
jupyter notebook usecases.ipynb
```

To use the MATLAB or R wrapper, see the Matlab and R folders respectively [Coming soon!].

## Authors

* **Ben Yetton**
* **Juan Antonio**
* **Junbai Hou**

## License

This project is licensed under the MIT License - see the [Licence](LICENSE.md) file for details

## Docs

See the [MednickDB Manual](https://docs.google.com/document/d/18tD_ddjSYGFzIE07Uzoi0E3woXQ61TnQjqc4uXM4eXg/edit?usp=sharing) for more information on the MednickDB itself.

API docs for this package are coming soon!!!