import pandas as pd
import sys
import os
my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path + '/../mednickdb_pyapi/')
sys.path.append(my_path + '/../mednickdb_pyapi')
sys.path.append(my_path + '/../')
from parsing_helper import wide_to_long, long_to_wide


def test_wide_to_long():
    wide_df = pd.read_excel(os.path.join(my_path,'testfiles/example_wide_format_data.xlsm'))
    wide_df.columns  = [col.replace('Pm','PM') for col in wide_df.columns]
    parse_format = {'visitid':{'V1':1,'V2':2,'V3':3}, 'sessionid':{'PM':'PM','AM':'AM'}}
    long_df = wide_to_long(wide_df, parse_format)
    num_subs = wide_df.shape[0]
    num_visits = 3
    num_sessions = 2
    assert long_df.shape[0] == num_subs*num_visits*num_sessions
    assert 'visitid' in long_df.columns
    assert 'sessionid' in long_df.columns
    assert 'subjectid' in long_df.columns
    assert tuple(long_df['visitid'].unique()) == (1,2,3)
    assert tuple(long_df['sessionid'].unique()) == ('AM','PM')


def test_long_to_wide():
    long_df = pd.read_csv(os.path.join(my_path, 'testfiles/example_long_format.csv'))
    wide_df = long_to_wide(long_df,
                           one_row_per='subjectid',
                           factor_variables=['visitid', 'sessionid'],
                           insert_factor_name_in_col_name=True)
    assert 'visitid' not in wide_df.columns
    assert 'sessionid' not in wide_df.columns
    assert 'subjectid' in wide_df.columns



