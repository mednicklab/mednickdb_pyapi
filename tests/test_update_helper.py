import sys
import os
import glob
my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path + '/../mednickdb_pyapi/')
sys.path.append(my_path + '/../mednickdb_pyapi')
sys.path.append(my_path + '/../')
from update_helper import run_update_helper
from pyapi import MednickAPI
import time


def input_mock(x, func: str):
    if x == 'Select A, B or C, or q to quit:':
        return func
    if x == 'Enter query to filter files on:':
        return 'studyid=TEST'
    if x == 'List them? y or n:':
        return 'y'
    if x == 'Continue? y or n:':
        return 'y'

def test_A(monkeypatch):
    med_api = MednickAPI(username=os.environ['MEDNICKDB_TESTING_EMAIL'], password=os.environ['MEDNICKDB_TESTING_PW'])

    file_info1_post = {
        'fileformat': 'sleep_scoring',
        'studyid': 'TEST',
        'versionid': 1,
        'subjectid': 2,
        'visitid': 1,
        'sessionid': 1,
        'filetype': 'sleep_scoring'
    }
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/scorefile1.mat'), 'rb') as scorefile1:
        fid1 = med_api.upload_file(scorefile1,
                                   **file_info1_post)

    file_info2_post = file_info1_post.copy()
    file_info2_post.update({'visitid': 2})
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/scorefile2.mat'), 'rb') as scorefile2:
        fid2 = med_api.upload_file(scorefile2,
                                   **file_info2_post)

    time.sleep(1)

    monkeypatch.setattr('builtins.input', lambda x: input_mock(x, 'A'))
    files_deleted = run_update_helper(med_api=med_api)
    assert {'scorefile1.mat', 'scorefile2.mat'} == set(med_api.extract_var(files_deleted,'filename'))

    time.sleep(0.5)
    deleted_files = med_api.get_deleted_files()
    assert 'scorefile1.mat' in med_api.extract_var(deleted_files, 'filename')

def test_C(monkeypatch):
    med_api = MednickAPI(username=os.environ['MEDNICKDB_TESTING_EMAIL'], password=os.environ['MEDNICKDB_TESTING_PW'])

    file_info1_post = {
        'fileformat': 'sleep_scoring',
        'studyid': 'TEST',
        'versionid': 1,
        'subjectid': 2,
        'visitid': 1,
        'sessionid': 1,
        'filetype': 'sleep_scoring'
    }
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/scorefile1.mat'), 'rb') as scorefile1:
        file_info_1 = med_api.upload_file(scorefile1,
                                   **file_info1_post)

    time.sleep(0.2)
    med_api.update_parsed_status(fid=file_info_1['_id'])
    file_info_1_updated = med_api.get_file_by_fid(fid=file_info_1['_id'])

    file_info2_post = file_info1_post.copy()
    file_info2_post.update({'visitid': 2})
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/scorefile2.mat'), 'rb') as scorefile2:
        file_info_2 = med_api.upload_file(scorefile2,
                                   **file_info2_post)

    time.sleep(0.2)
    med_api.update_parsed_status(fid=file_info_2['_id'], status=True)
    file_info_2_updated = med_api.get_file_by_fid(fid=file_info_2['_id'])

    time.sleep(0.2)

    parsed_files = med_api.get_parsed_files()
    assert file_info_2['_id'] in med_api.extract_var(parsed_files, '_id')
    assert file_info_1['_id'] in med_api.extract_var(parsed_files, '_id')

    monkeypatch.setattr('builtins.input', lambda x: input_mock(x, 'C'))
    files_updated = run_update_helper(med_api=med_api)
    assert {'scorefile1.mat', 'scorefile2.mat'} == set(med_api.extract_var(files_updated,'filename'))

    unparsed_files = med_api.get_unparsed_files()
    assert file_info_2['_id'] in med_api.extract_var(unparsed_files, '_id')
    assert file_info_1['_id'] in med_api.extract_var(unparsed_files, '_id')

    file_info_1_after_helper = med_api.get_file_by_fid(fid=file_info_1['_id'])
    file_info_2_after_helper = med_api.get_file_by_fid(fid=file_info_2['_id'])

    assert file_info_1_after_helper['datemodified'] > file_info_1_updated['datemodified']
    assert file_info_2_after_helper['datemodified'] > file_info_2_updated['datemodified']


def test_B(monkeypatch):
    med_api = MednickAPI(username=os.environ['MEDNICKDB_TESTING_EMAIL'], password=os.environ['MEDNICKDB_TESTING_PW'])

    file_info1_post = {
        'fileformat': 'sleep_scoring',
        'studyid': 'TEST',
        'versionid': 1,
        'subjectid': 2,
        'visitid': 1,
        'sessionid': 1,
        'filetype': 'sleep_scoring'
    }
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/scorefile1.mat'), 'rb') as scorefile1:
        fid1 = med_api.upload_file(scorefile1,
                                   **file_info1_post)

    file_info2_post = file_info1_post.copy()
    file_info2_post.update({'visitid': 2})
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/scorefile2.mat'), 'rb') as scorefile2:
        fid2 = med_api.upload_file(scorefile2,
                                   **file_info2_post)

    time.sleep(1)

    monkeypatch.setattr('builtins.input', lambda x: input_mock(x, 'B'))
    files_deleted = run_update_helper(med_api=med_api)
    assert {'scorefile1.mat', 'scorefile2.mat'} == set(med_api.extract_var(files_deleted,'filename'))

    time.sleep(0.5)

    deleted_files = med_api.get_deleted_files()
    assert 'scorefile1.mat' in med_api.extract_var(deleted_files, 'filename')

    for file in files_deleted:
        assert not os.path.exists(file['filepath'])
