import sys
import os
my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path+'/../')
from mednickdb_pyapi.pyapi import MednickAPI,ServerError,ResponseError
import pytest
import time
import json

env_list = ['MEDNICKDB_TESTING_EMAIL','MEDNICKDB_TESTING_PW','MEDNICKDB_SERVER_ADDRESS','MEDNICKDB_API_PORT_NUMBER']
if not all([env in os.environ for env in env_list]):
    raise ServerError('Unable to start testing due to the missing of one or more of MEDNICKDB_TESTING_EMAIL, MEDNICKDB_TESTING_PW, MEDNICKDB_SERVER_ADDRESS, MEDNICKDB_API_PORT_NUMBER environment variables')

user = os.environ['MEDNICKDB_TESTING_EMAIL']
password = os.environ['MEDNICKDB_TESTING_PW']
server_address = os.environ['MEDNICKDB_SERVER_ADDRESS']+':'+os.environ['MEDNICKDB_API_PORT_NUMBER']


@pytest.fixture(scope="module")
def mednickAPI_setup():
    return MednickAPI(user, password, server_address)


def recursive_predicate_val(d, key, val):
    """
    Recursively step into a dictionary and make sure that the key:value pair exists somewhere

    :param d: dict to go into
    :param key: key to check
    :param val: value to check
    :return: True if exists, False otherwise
    """
    if key in d:
        return d[key]==val
    for k in d:
        if type(d[k])==dict:
            if recursive_predicate_val(d[k], key, val):
                return True
    return False


def test_login(mednickAPI_setup):
    """Test login, this will always pass until we deal with login"""
    med_api = mednickAPI_setup
    assert med_api.usertype == 'Admin'
    assert med_api.s.get(server_address + '/auth/check').json()['authenticated'] == True;
'''
def test_get_nonexisted_file():
    med_api = MednickAPI(server_address, 'test_grad_account@uci.edu', 'Pass1234')
    fids = med_api.get_files(studyid='SOMETHING')
    assert not len(fids)

def test_get_test_file():
    med_api = MednickAPI(server_address, 'test_grad_account@uci.edu', 'Pass1234')
    fids = med_api.get_files(studyid='TEST')
    assert fids
'''


def test_clear_test_study(mednickAPI_setup):
    med_api = mednickAPI_setup
    fids = med_api.extract_var(med_api.get_files(studyid='TEST'), '_id')
    if fids:
        for fid in fids:
            med_api.delete_file(fid, delete_all_versions=True, remove_associated_data=True)
            time.sleep(0.25)
        fids2 = med_api.extract_var(med_api.get_files(studyid='TEST'),'_id')
        assert fids2 == []
        deleted_fids = med_api.extract_var(med_api.get_deleted_files(),'_id')
        assert all([dfid in deleted_fids for dfid in fids])
    med_api.delete_data(studyid='TEST')
    assert len(med_api.get_data(studyid='TEST', format='nested_dict')) == 0

    #TEST file GET after delete
    fids3 = med_api.get_files(studyid='TEST')
    assert not len(fids3)
    time.sleep(0.5)


@pytest.mark.dependency(['test_clear_test_study'])
def test_upload_and_download_file(mednickAPI_setup):
    """Uploaded a file and download it again and make sure it matches"""
    med_api = mednickAPI_setup
    #print(med_api.s.get('http://localhost:8000/auth/check').json())
    files_on_server_before_upload = med_api.get_files(studyid='TEST')
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/scorefile1.mat'), 'rb') as uploaded_version:
        file_info = med_api.upload_file(fileobject=uploaded_version,
                                        fileformat='dont_parse_me',
                                        studyid='TEST',
                                        subjectid=1,
                                        versionid=1,
                                        filetype='sleep')
        print(file_info['_id'])
        file_test = med_api.get_file_by_fid(file_info['_id'])
        downloaded_version = med_api.download_file(file_info['_id'])
    time.sleep(0.5)
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/scorefile1.mat'), 'rb') as uploaded_version:
        assert downloaded_version == uploaded_version.read()
    time.sleep(0.5)
    files_on_server_after_upload = med_api.get_files(studyid='TEST')
    assert len(files_on_server_before_upload)+1 == len(files_on_server_after_upload)


@pytest.mark.dependency(['test_clear_test_study'])
def test_upload_and_overwrite(mednickAPI_setup):
    """Test that a file uploaded with the same name and info overwrites the older version
    When a file with the same filename, and same location in the file servers is uploaded:
        - The previous version will be set as active=False
        - The new version will get a new FID
        -

    """
    med_api = mednickAPI_setup
    with open(os.path.join(os.path.dirname(__file__),'testfiles/TEST_Demographics.xlsx'), 'rb') as uploaded_version_1:
        file1_info_before_overwrite = med_api.upload_file(fileobject=uploaded_version_1,
                                   fileformat='dont_parse_me',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='unique_thing_1')
    downloaded_version_1 = med_api.download_file(file1_info_before_overwrite['_id'])
    file_version_before_overwrite = file1_info_before_overwrite['filename_version']

    with open(os.path.join(os.path.dirname(__file__),'testfiles/updated_versions/TEST_Demographics.xlsx'), 'rb') as uploaded_version_2:
        file1_info_after_overwrite = med_api.upload_file(fileobject=uploaded_version_2,
                                   fileformat='dont_parse_me',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='unique_thing_1')
    downloaded_version_2 = med_api.download_file(file1_info_after_overwrite['_id'])
    file_version_after_overwrite = file1_info_after_overwrite['filename_version']

    with open(os.path.join(os.path.dirname(__file__),'testfiles/updated_versions/TEST_Demographics.xlsx'), 'rb') as uploaded_version_2:
        f = uploaded_version_2.read()
        assert downloaded_version_2 == f
        assert downloaded_version_1 != f

    #Get all versions, and make sure both versions of the file match what was uploaded
    all_versions = med_api.get_files(filename='TEST_Demographics.xlsx', previous_versions=True)
    assert all([file in med_api.extract_var(all_versions, 'filename_version') for file in [file_version_after_overwrite, file_version_before_overwrite]])

    file = med_api.get_files(filename='TEST_Demographics.xlsx')
    assert len(file) == 1
    assert file1_info_before_overwrite['_id'] != file1_info_after_overwrite['_id'] #It gets a new fid
    assert file[0]['_id'] == file1_info_after_overwrite['_id']

    downloaded_version_current = med_api.download_file(file[0]['_id'])
    assert downloaded_version_current == downloaded_version_2
    assert downloaded_version_1 != downloaded_version_2

@pytest.mark.dependency(['test_upload_and_download_file'])
def test_update_file_info(mednickAPI_setup):
    med_api = mednickAPI_setup
    old_file = med_api.get_files(filename='TEST_Demographics.xlsx')[0]
    fid = old_file['_id']
    old_file_format = old_file['fileformat']
    old_file_type = old_file["filetype"]
    old_file_ver = old_file['versionid']
    old_file_sub_id = old_file['subjectid']
    old_file_study_id = old_file['studyid']

    #change fileinfo
    new_file_format = 'TestFormat'
    new_file_type = 'TestType'
    new_file_ver = 99
    new_file_sub_id= 99
    new_file_study_id = 'testFileInfoUpload'
    try:
        med_api.update_file_info(fid,
                                 fileformat=new_file_format,
                                 filetype=new_file_type,
                                 studyid=new_file_study_id,
                                 subjectid=new_file_sub_id,
                                 versionid=new_file_ver)
        new_file = med_api.get_files(filename='TEST_Demographics.xlsx')[0]
        new_fid = new_file['_id']

        assert new_file['fileformat'] == new_file_format
        assert new_file["filetype"] == new_file_type
        assert new_file['versionid'] == new_file_ver
        assert new_file['subjectid'] == new_file_sub_id
        assert new_file['studyid'] == new_file_study_id

        #revert change

        # med_api.update_file_info(new_fid,
        #                          fileformat=old_file_format,
        #                          filetype=old_file_type,
        #                          studyid=old_file_study_id,
        #                          subjectid=old_file_sub_id,
        #                          versionid=old_file_ver)
        #
        # assert med_api.get_files(filename='TEST_Demographics.xlsx')[0]['fileformat'] == old_file_format
        # assert med_api.get_files(filename='TEST_Demographics.xlsx')[0]["filetype"] == old_file_type
        # assert med_api.get_files(filename='TEST_Demographics.xlsx')[0]['versionid'] == old_file_ver
        # assert med_api.get_files(filename='TEST_Demographics.xlsx')[0]['subjectid'] == old_file_sub_id
        # assert med_api.get_files(filename='TEST_Demographics.xlsx')[0]['studyid'] == old_file_study_id
        #
        # # another test case
        # fids = med_api.get_files(studyid='TEST')
        # file_info_1 = med_api.get_file_by_fid(fid=fids[0]['_id'])
        # to_add = {'sessionid': 10}
        # med_api.update_file_info(fid=fids[0]['_id'], file_info=to_add)
        # file_info_1.update(to_add)
        # time.sleep(file_update_time)  # Give db 5 seconds to update
        #
        # file_info_2 = med_api.get_file_by_fid(fids[0]['_id'])
        # assert (file_info_2 == file_info_1)

    except NotImplementedError:
        return

@pytest.mark.dependency(['test_upload_and_download_file'])
def test_update_parsed_status(mednickAPI_setup):
    med_api = mednickAPI_setup
    with open(os.path.join(os.path.dirname(__file__),'testfiles/test_text_1.txt'), 'r') as text1:

        text1_info = med_api.upload_file(fileobject=text1,
                                   fileformat='txt',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='text_1')
    unparsed = med_api.get_files(studyid='TEST')
    unparsed_fid = set()
    for i in unparsed:
        if not i['parsed']:
            unparsed_fid.add(i['_id'])
    for id in unparsed_fid:
        med_api.update_parsed_status(id,True)
    files = med_api.get_files(studyid='TEST')
    for i in files:
        if i['_id'] in unparsed_fid:
            assert i['parsed']

    '''
    use the code bellow after update_parsed fully implemented
    '''

    test_files = med_api.get_files(studyid='TEST', fileformat='txt')
    fid_list = med_api.extract_var(test_files, "_id")
    parsed_status_list = med_api.extract_var(test_files,"parsed")

    for i in range(len(fid_list)):
        med_api.update_parsed_status(test_files[i]['_id'], not test_files[i]['parsed'])
    # time.sleep(5)
    new_files = med_api.get_files(studyid='TEST', fileformat='txt')

    # # check if new files are retrieved in the same order as the old files
    new_fid_list = med_api.extract_var(new_files, "_id")
    new_parsed_status_list = med_api.extract_var(new_files, "parsed")
    # # change parsed status shall not effect DB _id field
    assert fid_list == new_fid_list
    assert parsed_status_list == [i for i in map(lambda x:not x,new_parsed_status_list)]
    #
    # #revert
    # for i in range(len(fid_list)):
    #     med_api.update_parsed_status(fid_list[i], parsed_status_list[i])

def test_get_file_by_id(mednickAPI_setup):
    med_api = mednickAPI_setup
    old_files = med_api.get_files(studyid='TEST')
    old_files.sort(key = lambda x:x["_id"])

    fids = med_api.extract_var(old_files,"_id")
    fids.sort()
    for i in range(len(fids)):
        assert med_api.get_file_by_fid(fids[i])==old_files[i]

@pytest.mark.dependency(['test_upload_and_download_file'])
def test_download_files(mednickAPI_setup):
    med_api = mednickAPI_setup
    fids = []
    with open(os.path.join(os.path.dirname(__file__),'testfiles/test_text_1.txt'), 'r') as text1,\
            open(os.path.join(os.path.dirname(__file__),'testfiles/test_text_2.txt'), 'r') as text2:
        text1_info = med_api.upload_file(fileobject=text1,
                                   fileformat='txt',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='text_1')
        fids.append(text1_info['_id'])

        text2_info = med_api.upload_file(fileobject=text2,
                                   fileformat='txt',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='text_1')
        fids.append(text2_info['_id'])

        assert fids
        try:
            # not implemented yet
            # TODO remove try block after fixed
            files = med_api.download_files(fids)
            assert files[0] == text1.read()
            assert files[1] == text2.read()
        except:
            return


def test_delete_file_basic(mednickAPI_setup):
    med_api = mednickAPI_setup
    file = med_api.get_files(studyid='TEST')[0]
    fid = file['_id']
    assert file['active']
    assert not file['expired']
    med_api.delete_file(fid)
    try:
        med_api.get_file_by_fid(fid)
    except ServerError as err:
        s = (str(err)[16:-1])
        res = json.loads(s)
        assert res["error"]["status"]==400
        assert res["error"]["message"]=="ID did not belong to nay record"


def test_delete_file_all_version(mednickAPI_setup):
    med_api = mednickAPI_setup
    with open(os.path.join(os.path.dirname(__file__),'testfiles/TEST_Demographics.xlsx'), 'rb') as uploaded_version_1:
        ver1 = med_api.upload_file(fileobject=uploaded_version_1,
                                   fileformat='dont_parse_me',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='unique_thing_1')
    fid = ver1['_id']

    with open(os.path.join(os.path.dirname(__file__),'testfiles/updated_versions/TEST_Demographics.xlsx'), 'rb') as uploaded_version_2:
        ver2 = med_api.upload_file(fileobject=uploaded_version_2,
                                   fileformat='dont_parse_me',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='unique_thing_1')
    existed = med_api.get_files(filename='TEST_Demographics.xlsx')
    assert existed

    med_api.delete_file(fid,delete_all_versions=True)
    after_deletion = med_api.get_files(filename='TEST_Demographics.xlsx')
    assert not after_deletion

def test_delete_files_reactivate_previous(mednickAPI_setup):
    med_api = mednickAPI_setup
    with open(os.path.join(os.path.dirname(__file__),'testfiles/TEST_Demographics.xlsx'), 'rb') as uploaded_version_1:
        ver1 = med_api.upload_file(fileobject=uploaded_version_1,
                                   fileformat='dont_parse_me',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='unique_thing_1')
    fid = ver1['_id']
    med_api.update_parsed_status(fid)

    with open(os.path.join(os.path.dirname(__file__),'testfiles/updated_versions/TEST_Demographics.xlsx'), 'rb') as uploaded_version_2:
        ver2 = med_api.upload_file(fileobject=uploaded_version_2,
                                   fileformat='dont_parse_me',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='unique_thing_1')
    fid2 = ver2['_id']
    med_api.delete_file(fid2,reactivate_previous=True)

    assert not med_api.get_files(filename='TEST_Demographics.xlsx',
                                 fileformat='dont_parse_me',
                                 studyid='TEST',
                                 subjectid=1,
                                 versionid=1,
                                 filetype='unique_thing_1')[0]['parsed']

def test_delete_files_remove_associated_data(mednickAPI_setup):
    med_api = mednickAPI_setup
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/scorefile1.mat'), 'rb') as uploaded_version:
        file_info = med_api.upload_file(fileobject=uploaded_version,
                                        fileformat='dont_parse_me',
                                        studyid='TEST',
                                        subjectid=1,
                                        versionid=1,
                                        filetype='sleep')
    file1 = med_api.get_files(filename='scorefile1.mat',studyid='TEST')[0]
    fid = file1['_id']
    studyid = file1['studyid']
    verid = file1['versionid']
    ftype = file1['filetype']
    subid = file1['subjectid']
    med_api.upload_data({'hello':'world','valid':False},studyid,verid,ftype,fid,subid)

    d1 = med_api.get_data(format='nested_dict',studyid='TEST')
    med_api.delete_file(fid,remove_associated_data=True)
    data = med_api.get_data_from_single_file(ftype, fid, format='nested_dict')
    assert not data

def test_delete_multiple_and_get_delete_files(mednickAPI_setup):
    med_api = mednickAPI_setup
    files = med_api.get_files(studyid='TEST', filetype='text_1')
    fids_to_delete = med_api.extract_var(files,"_id")

    med_api.delete_multiple(fids_to_delete)
    assert not med_api.get_files(studyid='TEST', filetype='text_1')
    deleted_fids = med_api.extract_var(med_api.get_deleted_files(), "_id")
    for id in fids_to_delete:
        assert id in deleted_fids


@pytest.mark.dependency(['test_upload_and_download_file'])
def test_upload_data_and_get_data(mednickAPI_setup):
    med_api = mednickAPI_setup
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/scorefile1.mat'), 'rb') as uploaded_version:
        file_info = med_api.upload_file(fileobject=uploaded_version,
                                        fileformat='dont_parse_me',
                                        studyid='TEST',
                                        subjectid=1,
                                        versionid=1,
                                        filetype='sleep')
    file1 = med_api.get_files(filename='scorefile1.mat',studyid='TEST')[0]
    fid = file1['_id']
    studyid = file1['studyid']
    verid = file1['versionid']
    ftype = file1['filetype']
    subid = file1['subjectid']
    med_api.upload_data({'hello':'world','valid':False},studyid,verid,ftype,fid,subid)

    d1 = med_api.get_data(format='nested_dict',studyid='TEST')

    assert any([recursive_predicate_val(i, 'hello', 'world') for i in d1])
    assert any([recursive_predicate_val(i, 'valid', False) for i in d1])
    assert not any([recursive_predicate_val(i, 'hello1', 'world') for i in d1])

def test_discard_subsets(mednickAPI_setup):
    med_api = mednickAPI_setup
    file1 = med_api.get_files(filename='TEST_Demographics.xlsx', studyid='TEST')[0]
    fid = file1['_id']
    studyid = file1['studyid']
    verid = file1['versionid']
    ftype = file1['filetype']
    subid = file1['subjectid']
    visitid = 1
    sessionid = 2

    # upload data1 (superset, has visitid & sessionid)
    med_api.upload_data({'hello':'world','valid':False},studyid,verid,ftype,fid,subid,visitid,sessionid)

    # upload data2 (subset, visitid but no sessionid)
    med_api.upload_data({"age":25},studyid,verid,ftype,fid,subid,visitid)

    # upload data3 (subset, no visitid or sessionid)
    med_api.upload_data({'new':'data'},studyid,verid,ftype,fid,subid)

    d1 = med_api.get_data(format='nested_dict', query='studyid=TEST and subjectid=1 and versionid=1')
    d2 = med_api.get_data(discard_subsets=False, format='nested_dict', query='studyid=TEST and subjectid=1 and versionid=1')

    assert len(d1) == 1
    assert any([recursive_predicate_val(i, 'new', 'data') for i in d1])
    assert any([recursive_predicate_val(i, 'age', 25) for i in d1])
    assert any([recursive_predicate_val(i, 'visitid', 1) for i in d1])
    assert any([recursive_predicate_val(i, 'sessionid', 2) for i in d1])

    assert len(d2) == 3

def test_data_overwrite(mednickAPI_setup):
    med_api = mednickAPI_setup
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/test_text_1.txt'), 'r') as text1:

        file_info2 = med_api.upload_file(fileobject=text1,
                                         fileformat='txt',
                                         studyid='TEST',
                                         subjectid=5,
                                         versionid=5,  # chamge verid to 5
                                         filetype='unique_thing_2')
        fid = file_info2['_id']
        med_api.upload_data({'age':22,"edu":8,"bmi":2},"TEST",5,'unique_thing_2',fid,5)
        kargs = {"studyid":"TEST","subjectid":5,"versionid":5}
        data = med_api.get_data_from_single_file('unique_thing_2',fid,format='nested_dict')
        record = data[0]

        age = record['data']['unique_thing_2']['age']
        edu = record['data']['unique_thing_2']['edu']
        bmi = record['data']['unique_thing_2']['bmi']
        assert age==22
        assert edu==8
        assert bmi==2
        med_api.upload_data({'age':32,"edu":10,"bmi":1},"TEST",5,'unique_thing_2',fid,5)
        data = med_api.get_data_from_single_file('unique_thing_2',fid,format='nested_dict')
        record = data[0]

        assert 32 == record['data']['unique_thing_2']['age']
        assert 10 == record['data']['unique_thing_2']['edu']
        assert 1 == record['data']['unique_thing_2']['bmi']





def test_delete_data(mednickAPI_setup):
    med_api = mednickAPI_setup
    dl1 = med_api.get_data(format='nested_dict', studyid='TEST')
    data_id = med_api.extract_var(dl1, '_id')

    to_delete = data_id[0]
    med_api.delete_data(id=to_delete)
    dl2 = med_api.get_data(format='nested_dict', studyid='TEST')

    assert to_delete not in med_api.extract_var(dl2, '_id')



@pytest.mark.dependency(['test_upload_data_and_get_data'])
def test_get_data_from_single_file(mednickAPI_setup):
    med_api = mednickAPI_setup
    file = med_api.get_files(studyid='TEST')[0]

    fid = file['_id']
    studyid = file['studyid']
    verid = file['versionid']
    ftype = file['filetype']
    subid = file['subjectid']
    med_api.upload_data({'hello':'world','valid':False,'point':3.5},studyid,verid,ftype,fid,subid)
    d = med_api.get_data_from_single_file(file['filetype'],file['_id'],format='nested_dict')

    assert any([recursive_predicate_val(i, 'hello', 'world') for i in d])
    assert any([recursive_predicate_val(i, 'valid', False) for i in d])
    assert any([recursive_predicate_val(i, 'point', 3.5) for i in d])


@pytest.mark.dependency(['test_update_parsed_status'])
def test_get_parsed_files(mednickAPI_setup):
    med_api = mednickAPI_setup
    unparsed_files = med_api.get_files(studyid='TEST', filetype='text_1')
    unparsed_fids = med_api.extract_var(unparsed_files,"_id")
    for i in unparsed_fids:
        med_api.update_parsed_status(i, True)
    retrieved_fids = med_api.extract_var(med_api.get_parsed_files(), "_id")
    for i in unparsed_fids:
        assert i in retrieved_fids

    #revert change
    # for i in unparsed_fids:
    #     med_api.update_parsed_status(i, False)

def test_get_delete_files(mednickAPI_setup):
    test_clear_test_study(mednickAPI_setup)
    med_api = mednickAPI_setup
    #files = med_api.get_files(studyid='TEST', filetype='text_1')
    with open(os.path.join(os.path.dirname(__file__),'testfiles/test_text_1.txt'), 'r') as text1:
        text1_info = med_api.upload_file(fileobject=text1,
                                   fileformat='txt',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='text_1')
    fid = text1_info['_id']
    med_api.delete_file(fid)
    fids = med_api.get_files(fileformat='txt',
                             studyid='TEST',
                             subjectid=1,
                             versionid=1,
                             filetype='text_1')
    fids = med_api.extract_var(fids,'_id')
    time.sleep(0.25)
    assert fid not in fids


def test_get_unique_var_values(mednickAPI_setup):
    test_clear_test_study(mednickAPI_setup)
    med_api = mednickAPI_setup
    with open(os.path.join(os.path.dirname(__file__),'testfiles/test_text_1.txt'), 'r') as text1,\
            open(os.path.join(os.path.dirname(__file__),'testfiles/test_text_2.txt'), 'r') as text2:
        text1_info = med_api.upload_file(fileobject=text1,
                                   fileformat='txt',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='text_1')

        text2_info = med_api.upload_file(fileobject=text2,
                                   fileformat='txt',
                                   studyid='TEST',
                                   subjectid=2,
                                   versionid=1,
                                   filetype='text_2')
    time.sleep(1)
    vals1 = med_api.get_unique_var_values('filetype','files',studyid='TEST')
    vals1.sort()
    if vals1 != ['text_1', 'text_2']:
        print('fail!')
    assert vals1 == ['text_1', 'text_2']
    vals2 = med_api.get_unique_var_values('subjectid','files',studyid='TEST')
    vals2.sort()
    assert vals2 == [1,2]

@pytest.mark.dependency(['test_update_parsed_status','test_get_unique_var_values'])
def test_get_unparsed_files(mednickAPI_setup):
    #test_clear_test_study(mednickAPI_setup)

    ##rely on previous test to clear db and populate new data

    med_api = mednickAPI_setup
    unparsed_files = med_api.get_files(studyid='TEST')
    fid_to_skip = []
    temp = []

    # retrieve unparsed file through get_file
    for i in range(len(unparsed_files)):
        if unparsed_files[i]['parsed']:
            print(unparsed_files[i]['_id'])
            fid_to_skip.append(i)

    # extract fileinfo of unparsed files
    for i in range(len(unparsed_files)):
        if i not in fid_to_skip:
            temp.append(unparsed_files[i])
    unparsed_files = temp
    unparsed_fids = med_api.extract_var(unparsed_files,"_id")
    unparsed_fids.sort()

    # get_unparsed_file call
    unparsed_files = med_api.get_unparsed_files()
    unparsed_files = [file for file in unparsed_files if file['studyid']=='TEST' and file['versionid']==1]
    retrieved_fids = med_api.extract_var(unparsed_files, "_id")
    retrieved_fids.sort()

    # print(unparsed_fids,"\n")
    # print(retrieved_fids)
    assert unparsed_fids == retrieved_fids


def test_file_operation_null_field(mednickAPI_setup):
    test_clear_test_study(mednickAPI_setup)
    med_api = mednickAPI_setup

    with open(os.path.join(os.path.dirname(__file__), 'testfiles/test_text_1.txt'), 'r') as text1:

        # studyid==null
        try:
            file_info2 = med_api.upload_file(fileobject=text1,
                                         fileformat="txt",
                                         studyid=None,
                                         subjectid=None,
                                         versionid=1,  # chamge verid to 5
                                         filetype="unique_thing_3")
        except ServerError as err:
            s = (str(err)[16:-1])
            res = json.loads(s)
            assert res["error"]["status"]==400
            assert res["error"]["message"]=="No studyid was included."
        else:
            assert 0, "This testcase should raise server error"


        #versionid
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/test_text_1.txt'), 'r') as text1:

        try:
            file_info2 = med_api.upload_file(fileobject=text1,
                                         fileformat="txt",
                                         studyid="TEST",
                                         subjectid=None,
                                         versionid=None,  # chamge verid to 5
                                         filetype="unique_thing_3")
        except ServerError as err:
            s = (str(err)[16:-1])
            res = json.loads(s)
            assert res["error"]["status"]==400
            assert res["error"]["message"]=="No versionid was included."
        else:
            assert 0, "This testcase should raise server error"


        #filetype
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/test_text_1.txt'), 'r') as text1:

        try:
            file_info2 = med_api.upload_file(fileobject=text1,
                                         fileformat="txt",
                                         studyid="TEST",
                                         subjectid=None,
                                         versionid=1,  # chamge verid to 5
                                         filetype=None)
        except ServerError as err:
            s = (str(err)[16:-1])
            res = json.loads(s)
            assert res["error"]["status"]==400
            assert res["error"]["message"]=="No filetype was included."
        else:
            assert 0, "This testcase should raise server error"

        #fileformat
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/test_text_1.txt'), 'r') as text1:

        try:
            file_info2 = med_api.upload_file(fileobject=text1,
                                         fileformat=None,
                                         studyid="TEST",
                                         subjectid=None,
                                         versionid=1,  # chamge verid to 5
                                         filetype="unique_thing_3")
        except ServerError as err:
            s = (str(err)[16:-1])
            res = json.loads(s)
            assert res["error"]["status"]==400
            assert res["error"]["message"]=="No fileformat was included."
        else:
            assert 0, "This testcase should raise server error"

        #all
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/test_text_1.txt'), 'r') as text1:

        try:
            file_info2 = med_api.upload_file(fileobject=text1,
                                         fileformat=None,
                                         studyid=None,
                                         subjectid=None,
                                         versionid=None,  # chamge verid to 5
                                         filetype=None)
        except ServerError as err:
            s = (str(err)[16:-1])
            res = json.loads(s)
            assert res["error"]["status"]==400
            assert res["error"]["message"]=="No studyid was included."
        else:
            assert 0,"This testcase should raise server error"



def test_data_operation_null_field(mednickAPI_setup):
    med_api = mednickAPI_setup
    with open(os.path.join(os.path.dirname(__file__), 'testfiles/test_text_1.txt'), 'r') as text1:

        file_info2 = med_api.upload_file(fileobject=text1,
                                             fileformat="txt",
                                             studyid="TEST",
                                             subjectid=None,
                                             versionid=1,  # chamge verid to 5
                                             filetype="unique_thing_3")
        fid = file_info2['_id']
        #studyid
        try:
            med_api.upload_data({'age': 22, "edu": 8, "bmi": 2}, None, 5, 'unique_thing_2', fid, 5)

        except ServerError as err:
            s = (str(err)[16:-1])
            res = json.loads(s)
            assert res["message"] == "No studyid was included."
        else:
            assert 0,"Query with Null studyid should raise server error"

        #filetype
        try:

            med_api.upload_data({'age': 22, "edu": 8, "bmi": 2}, "TEST", 5, None, fid, 5)

        except ServerError as err:
            s = (str(err)[16:-1])
            res = json.loads(s)
            assert res["message"] == "No filetype was included."
        else:
            assert 0, "Query with Null filetype should raise server error"


        #subjectid
        try:

            med_api.upload_data({'age': 22, "edu": 8, "bmi": 2}, "TEST", 5, "unique_thing_3", fid, None)

        except ServerError as err:
            s = (str(err)[16:-1])
            res = json.loads(s)
            assert res["message"] == "No subjectid was included."
        else:
            assert 0, "Query with Null subjectid should raise server error"


        #versionid
        try:

            med_api.upload_data({'age': 22, "edu": 8, "bmi": 2}, "TEST", None, "unique_thing_3", fid, 5)

        except ServerError as err:
            s = (str(err)[16:-1])
            res = json.loads(s)
            assert res["message"] == "No versionid was included."
        #TODO uncomment when fixed
        # else:
        #     assert 0, "Query with Null versionid should raise server error"




def test_upload_and_get_long_name_file(mednickAPI_setup):
    med_api = mednickAPI_setup
    with open(os.path.join(os.path.dirname(__file__),'testfiles/longname.ipynb'), 'rb') as longfile:
        fformat = 'jupyternb'
        ftype = 'ML'
        med_api.upload_file(fileobject=longfile,
                                   fileformat=fformat,
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype=ftype)
        f = med_api.get_files(fileformat=fformat,
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype=ftype)
        f = f[0]
        assert fformat == f['fileformat']
        assert ftype == f['filetype']

        assert 'longname.ipynb'==f['filename'] #the one fails

@pytest.mark.dependency(['test_upload_and_overwrite'])
def test_file_query(mednickAPI_setup):
    """Upload a bunch of files to the server, and query them using all the types of querying available"""
    test_clear_test_study(mednickAPI_setup)  # Start Fresh
    med_api = mednickAPI_setup
    with open(os.path.join(os.path.dirname(__file__),'testfiles/scorefile1.mat'), 'rb') as uploaded_version:
        file_info1 = med_api.upload_file(fileobject=uploaded_version,
                                  fileformat='dont_parse_me',
                                  studyid='TEST',
                                  subjectid=1,
                                  versionid=1,
                                  filetype='sleep')
        fid1 = file_info1['_id']

    with open(os.path.join(os.path.dirname(__file__),'testfiles/scorefile1.mat'), 'rb') as uploaded_version:
        file_info2 = med_api.upload_file(fileobject=uploaded_version,
                                  fileformat='dont_parse_me',
                                  studyid='TEST',
                                  subjectid=2,
                                  versionid=1,
                                  filetype='sleep')
        fid2 = file_info2['_id']

    with open(os.path.join(os.path.dirname(__file__),'testfiles/TEST_Demographics.xlsx'), 'rb') as uploaded_version_1:
        file_info3 = med_api.upload_file(fileobject=uploaded_version_1,
                                   fileformat='dont_parse_me',
                                   studyid='TEST',
                                   subjectid=3,
                                   versionid=2,
                                   filetype='unique_thing_2')
        fid3 = file_info3['_id']

    time.sleep(1)

    #Test ==
    fids = med_api.extract_var(med_api.get_files(query='studyid==TEST'),'_id')
    assert all([fid in fids for fid in [fid1, fid2, fid3]])

    #Test IN
    fids = med_api.extract_var(med_api.get_files(query='studyid=TEST and subjectid in [1,2]'), '_id')
    assert all([fid in fids for fid in [fid1, fid2]])

    #Test not in
    fids = med_api.extract_var(med_api.get_files(query='studyid=TEST and subjectid not in [1,2]'), '_id')
    assert all([fid in fids for fid in [fid3]])

    # Test and
    fids = med_api.extract_var(med_api.get_files(query='studyid=TEST and subjectid==1 and versionid==1'), '_id')
    assert all([fid in fids for fid in [fid1]])

    # Test or
    fids = med_api.extract_var(med_api.get_files(query='studyid=TEST and subjectid==2 or 1'),'_id')
    assert all([fid in fids for fid in [fid1, fid2]])

    #Test not =
    fids = med_api.extract_var(med_api.get_files(query='studyid=TEST and subjectid!=2'),'_id')
    assert all([fid in fids for fid in [fid1, fid3]])

    #Test >
    fids = med_api.extract_var(med_api.get_files(query='studyid=TEST and subjectid>2'),'_id')
    assert all([fid in fids for fid in [fid3]])

    #Test <
    fids = med_api.extract_var(med_api.get_files(query='studyid=TEST and subjectid<2'),'_id')
    assert all([fid in fids for fid in [fid1]])

    #Test <=
    fids = med_api.extract_var(med_api.get_files(query='studyid=TEST and subjectid<=2'),'_id')
    assert all([fid in fids for fid in [fid1, fid2]])

    #Test <=
    fids = med_api.extract_var(med_api.get_files(query='studyid=TEST and subjectid>=2'),'_id')
    assert all([fid in fids for fid in [fid2, fid3]])

    # Test complex
    fids = med_api.extract_var(med_api.get_files(query='studyid=TEST and subjectid>2 or <=1'),'_id')
    assert all([fid in fids for fid in [fid1, fid3]])

@pytest.mark.dependency(['test_upload_data','test_data_overwrite'])
def test_data_query(mednickAPI_setup):
    import time
    med_api = mednickAPI_setup
    test_clear_test_study(mednickAPI_setup)

    def strip_non_matching_keys(strip_from, template):
        return {k: v for k, v in strip_from.items() if k in template}

    with open(os.path.join(os.path.dirname(__file__),'testfiles/TEST_Demographics.xlsx'), 'rb') as uploaded_version_1:
        file_info1 = med_api.upload_file(fileobject=uploaded_version_1,
                                         fileformat='dont_parse_me',
                                         studyid='TEST',
                                         subjectid=1,
                                         versionid=2,
                                         filetype='unique_thing_3')
        fid1 = file_info1['_id']
    time.sleep(20) #TODO remove this
    row1 = {'sex':'M', 'age':22, 'edu':12}
    row2 = {'sex':'F', 'age':19, 'edu':8}
    row3 = {'sex':'M', 'age':29, 'edu':18}
    med_api.upload_data(data=row1,
                        studyid='TEST',
                        subjectid=1,
                        versionid=1,
                        visitid=1,
                        filetype='demographics',
                        fid=fid1)

    med_api.upload_data(data=row2,
                        studyid='TEST',
                        subjectid=2,
                        versionid=1,
                        visitid=2,
                        filetype='demographics',
                        fid=fid1
                        )

    med_api.upload_data(data=row3,
                        studyid='TEST',
                        subjectid=3,
                        versionid=1,
                        filetype='demographics',
                        fid=fid1)

    time.sleep(0.5)

    # sanity check to see if we have any data at all:
    data_rows = med_api.get_data(format='nested_dict', studyid='TEST')
    assert len(data_rows) > 0

    #Test filetype=
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict', query='studyid=TEST and filetype = demographics')]
    assert all([row in data_rows for row in [row1, row2, row3]])

    #Test filetype =
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict', query='studyid=TEST and filetype=demographics')]
    assert all([row in data_rows for row in [row1, row2, row3]])

    # Test ==
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict',query='studyid=TEST and data.demographics.sex==M')]
    assert all([row in data_rows for row in [row1]])

    # Test IN
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict',query='studyid=TEST and data.demographics.age in [22,19]')]
    assert all([row in data_rows for row in [row1, row2]])

    # Test not in
    d_rows = med_api.get_data(format='nested_dict',query='studyid=TEST and data.demographics.age not in [22,19]')
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in d_rows]
    assert all([row in data_rows for row in [row3]])

    # Test and
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict',query='studyid=TEST and data.demographics.age==22 and versionid==1')]
    assert all([row in data_rows for row in [row1]])

    # Test or
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict',query='studyid=TEST and data.demographics.age==22 or 19')]
    assert all([row in data_rows for row in [row1, row2]])

    # Test not =
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict',query='studyid=TEST and data.demographics.age!=22')]
    assert all([row in data_rows for row in [row2, row3]])

    # Test >
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict',query='studyid=TEST and data.demographics.age>19')]
    assert all([row in data_rows for row in [row1, row3]])

    # Test <
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict',query='studyid=TEST and data.demographics.age<22')]
    assert all([row in data_rows for row in [row2]])

    # Test <=
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict',query='studyid=TEST and data.demographics.age>=22')]
    assert all([row in data_rows for row in [row1, row3]])

    # Test >=
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict',query='studyid=TEST and data.demographics.age<=22')]
    assert all([row in data_rows for row in [row1, row2]])

    # Test complex or
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict',query='studyid=TEST and data.demographics.age<22 or >28')]
    assert all([row in data_rows for row in [row2, row3]])

    # Test complex filetype
    data_rows = [strip_non_matching_keys(row['data']['demographics'], row1) for row in med_api.get_data(format='nested_dict',query='studyid=TEST and filetype=demographics and subjectid>2')]
    assert all([row in data_rows for row in [row3]])


# def test_update_file_info(): #TODO alternative for update_file_info
#     med_api = MednickAPI(server_address, 'test_grad_account@uci.edu', 'Pass1234')
#     fids = med_api.get_files(studyid='TEST')
#     file_info_1 = med_api.get_file_by_fid(fid=fids[0]['_id'])
#     to_add = {'sessionid': 10}
#     med_api.update_file_info(fid=fids[0]['_id'], file_info=to_add)
#     file_info_1.update(to_add)
#     time.sleep(file_update_time)  # Give db 5 seconds to update
#
#     file_info_2 = med_api.get_file_by_fid(fids[0]['_id'])
#     assert (file_info_2 == file_info_1)
#
#

def test_parsing_status(mednickAPI_setup):
    med_api = mednickAPI_setup
    fids = med_api.get_files(studyid='TEST')
    med_api.update_parsed_status(fids[0]['_id'], False)
    time.sleep(0.1) #cant wait too long otherwise pyparse will parse this...
    fids2 = med_api.get_unparsed_files()
    assert (fids[0]['_id'] in med_api.extract_var(fids2,'_id'))
    med_api.update_parsed_status(fids[0]['_id'], True)
    time.sleep(0.1)
    fids3 = med_api.get_parsed_files()
    assert (fids[0]['_id'] in med_api.extract_var(fids3,'_id'))