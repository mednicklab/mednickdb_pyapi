import sys
import os
my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path+'/../')
from mednickdb_pyapi.pyapi import MednickAPI
import pytest
import time

env_list = ['MEDNICKDB_TESTING_EMAIL','MEDNICKDB_TESTING_PW','MEDNICKDB_SERVER_ADDRESS','MEDNICKDB_API_PORT_NUMBER']
if not all([env in os.environ for env in env_list]):
    raise ValueError('Unable to start testing due to the missing of one or more of MEDNICKDB_TESTING_EMAIL, MEDNICKDB_TESTING_PW, MEDNICKDB_SERVER_ADDRESS, MEDNICKDB_API_PORT_NUMBER environment variables')

user = os.environ['MEDNICKDB_TESTING_EMAIL']
password = os.environ['MEDNICKDB_TESTING_PW']
server_address = os.environ['MEDNICKDB_SERVER_ADDRESS']+':'+os.environ['MEDNICKDB_API_PORT_NUMBER']

@pytest.fixture(scope="module")
def mednickAPI_setup():
    return MednickAPI(user, password, server_address)


def test_clear_test_study(mednickAPI_setup):
    med_api = mednickAPI_setup
    fids = med_api.extract_var(med_api.get_files(studyid='TEST'), '_id')
    if fids:
        for fid in fids:
            med_api.delete_file(fid, delete_all_versions=True, remove_associated_data=True)
            time.sleep(0.25)
        fids2 = med_api.extract_var(med_api.get_files(studyid='TEST'),'_id')
        assert fids2 == []
        deleted_fids = med_api.extract_var(med_api.get_deleted_files(),'_id')
        assert all([dfid in deleted_fids for dfid in fids])
    med_api.delete_data(studyid='TEST')
    assert len(med_api.get_data(studyid='TEST', format='nested_dict')) == 0

    #TEST file GET after delete
    fids3 = med_api.get_files(studyid='TEST')
    assert not len(fids3)
    time.sleep(0.5)

def test_get_delete_files(mednickAPI_setup):
    test_clear_test_study(mednickAPI_setup)
    med_api = mednickAPI_setup
    #files = med_api.get_files(studyid='TEST', filetype='text_1')
    with open(os.path.join(os.path.dirname(__file__),'test_text_1.txt'), 'r') as text1:
        text1_info = med_api.upload_file(fileobject=text1,
                                   fileformat='txt',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='text_1')
    fid = text1_info['_id']
    med_api.delete_file(fid)
    fids = med_api.get_files(fileformat='txt',
                             studyid='TEST',
                             subjectid=1,
                             versionid=1,
                             filetype='text_1')
    fids = med_api.extract_var(fids,'_id')
    time.sleep(0.25)
    assert fid not in fids


def test_get_unique_var_values(mednickAPI_setup):
    test_clear_test_study(mednickAPI_setup)
    med_api = mednickAPI_setup
    with open(os.path.join(os.path.dirname(__file__),'test_text_1.txt'), 'r') as text1:
        text1_info = med_api.upload_file(fileobject=text1,
                                   fileformat='txt',
                                   studyid='TEST',
                                   subjectid=1,
                                   versionid=1,
                                   filetype='text_1')

    time.sleep(1)
    vals1 = med_api.get_unique_var_values('filetype','files',studyid='TEST')
    vals1.sort()
    if vals1 != ['text_1']:
        print('fail!')
    assert vals1 == ['text_1']