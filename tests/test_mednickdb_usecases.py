import sys
import os
my_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(my_path+'/../')
from mednickdb_pyapi.pyapi import MednickAPI
import pytest
import time
from datetime import datetime
import pprint
import pickle
pp = pprint.PrettyPrinter(indent=4)

server_address = 'http://saclab.ss.uci.edu:8000'

file_update_time = 2
data_update_time = 10

user = 'mednickdb.testing@gmail.com'
password = os.environ['MEDNICKDB_DEFAULT_PW']

data_upload_working = False

@pytest.fixture(scope="module")
def med_api():
    return MednickAPI(user, password)


def dict_issubset(superset, subset, show_diffs=False):
    if show_diffs:
        return [item for item in subset.items() if item not in superset.items()]
    return all(item in superset.items() for item in subset.items())


def pytest_namespace(): #FIXME remove?
    return {'usecase_1_filedata': None}


def remove_all_study_settings(med_api):
    study_settings = med_api.get_files(filetype='study_settings')
    for file in study_settings:
        med_api.delete_file(fid=file['_id'], delete_all_versions=True)


def test_clear_test_study(med_api):
    """
    Clear all data and files with the studyid of "TEST". This esentually refreshes the database for new testing.
    """
    fids = med_api.extract_var(med_api.get_files(studyid='TEST'), '_id')
    if fids:
        for fid in fids:
            med_api.delete_file(fid, delete_all_versions=True)
            med_api.delete_data_from_single_file(fid)
        fids2 = med_api.extract_var(med_api.get_files(studyid='TEST'),'_id')
        assert fid not in fids2
        assert (fids2 == [])
        deleted_fids = med_api.extract_var(med_api.get_deleted_files(),'_id')
        assert all([dfid in deleted_fids for dfid in fids])
    med_api.delete_data(studyid='TEST')
    assert len(med_api.get_data(studyid='TEST', format='nested_dict')) == 0 #TODO after clearing up sourceid bug


@pytest.mark.dependency(['test_clear_test_study', 'test_study_settings_upload'])
def test_usecase_1(med_api):
    """runs usecase one from the mednickdb_usecase document (fid=)"""
    #a) done by fixture

    # some hackery to get these raw_eeg files to parse
    remove_all_study_settings(med_api)
    study_settings_file = os.path.join(my_path,'testfiles/Sleepfile1_study_settings.yaml')
    with open(study_settings_file, 'rb') as f:
        study_settings_info = med_api.upload_file(fileobject=f, filetype='study_settings', fileformat='study_settings', studyid='TEST', versionid=1)

    file_info_post = {
        'fileformat':'raw_eeg',
        'studyid':'TEST',
        'versionid':1,
        'subjectid':1,
        'visitid':1,
        'sessionid':1,
        'filetype':'raw_eeg',
    }
    file_data_real = file_info_post.copy()
    with open(os.path.join(os.path.dirname(__file__),'testfiles/sleepfile1.edf'),'rb') as sleepfile:
        file_info_returned = med_api.upload_file(fileobject=sleepfile, **file_info_post)

    with open(os.path.join(os.path.dirname(__file__),'testfiles/sleepfile1.edf'), 'rb') as sleepfile:
        downloaded_sleepfile = med_api.download_file(file_info_returned['_id'])
        assert (downloaded_sleepfile == sleepfile.read())

    # b)
    time.sleep(file_update_time)  # give db 5 seconds to update
    file_info_get = med_api.get_file_by_fid(file_info_returned['_id'])
    file_info_post.update({'filename': 'sleepfile1.edf', 'filedir': 'uploads/TEST/1/1/1/1/raw_eeg/'})
    assert dict_issubset(file_info_get, file_info_post)

    time.sleep(data_update_time-file_update_time)  # give db 5 seconds to update
    file_datas = med_api.get_data_from_single_file(filetype='raw_eeg', fid=file_info_returned['_id'], format='flat_dict')
    assert len(file_datas) > 0, "Is pyparse running?"
    file_data_real.pop('fileformat')
    file_data_real.pop('filetype')
    file_data_real.update({'raw_eeg.sfreq':128, 'raw_eeg.start_datetime': datetime(2003, 1, 1, 0, 25, 37), 'raw_eeg.ch_names': ['C3A2', 'C4A1', 'ECG']})
    pytest.usecase_1_filedata = file_data_real
    pytest.usecase_1_filename_version = file_info_get['filename_version']


    assert any([dict_issubset(file_data, file_data_real) for file_data in file_datas]), "Is pyparse running? (and working)"


    med_api.delete_file(fid=study_settings_info['_id'], delete_all_versions=True)

@pytest.mark.dependency(['test_usecase_1'])
def test_usecase_2(med_api):
    # a)

    file_info_post = {'filetype':'demographics',
                      'fileformat':'tabular',
                      'studyid':'TEST',
                      'versionid':1}

    with open(os.path.join(os.path.dirname(__file__),'testfiles/TEST_Demographics.xlsx'), 'rb') as demofile:
        # b)
        file_info = med_api.upload_file(fileobject=demofile, **file_info_post)
        fid = file_info['_id']
        downloaded_demo = med_api.download_file(fid)
        with open(os.path.join(os.path.dirname(__file__),'testfiles/TEST_Demographics.xlsx'), 'rb') as demofile:
            assert downloaded_demo == demofile.read()

    # c)
    time.sleep(file_update_time)  # Give file db 5 seconds to update
    file_info_post.update({'filename': 'TEST_Demographics.xlsx', 'filedir': 'uploads/TEST/1/demographics/'})
    file_info_get = med_api.get_file_by_fid(fid)
    assert dict_issubset(file_info_get, file_info_post)

    # d)
    time.sleep(data_update_time-file_update_time)  # Give data db 50 seconds to update
    data_rows = med_api.get_data(studyid='TEST', versionid=1, format='flat_dict')
    correct_row1 = {'studyid': 'TEST', 'versionid': 1, 'subjectid': 1,
                    'demographics.age': 23, 'demographics.sex': 'F', 'demographics.bmi': 23}
    correct_row1.update(pytest.usecase_1_filedata)
    correct_row2 = {'studyid': 'TEST', 'versionid': 1, 'subjectid': 2,
                    'demographics.age': 19, 'demographics.sex': 'M', 'demographics.bmi': 20}
    correct_rows = [correct_row1, correct_row2]

    pytest.usecase_2_row1 = correct_row1
    pytest.usecase_2_row2 = correct_row2
    pytest.usecase_2_filename_version = file_info_get['filename_version']

    for correct_row in correct_rows:
        assert any([dict_issubset(data_row, correct_row) for data_row in data_rows]), "demographics data downloaded does not match expected"

    # e)
    data_sleep_eeg = med_api.get_data(studyid='TEST', versionid=1, filetype='raw_eeg', format='flat_dict')[0]
    assert dict_issubset(data_sleep_eeg, pytest.usecase_1_filedata), "eeg data downloaded does not match what was uploaded in usecase 1"


@pytest.mark.dependency(['test_usecase_2'])
def test_usecase_3(med_api):

    # a)
    fid_for_manual_upload = med_api.extract_var(med_api.get_files(studyid='TEST'), '_id')[0] # get a random fid
    data_post = {'studyid': 'TEST',
                 'filetype': 'memtesta',
                 'data': {'accuracy': 0.9},
                 'versionid': 1,
                 'subjectid': 2,
                 'visitid': 1,
                 'sessionid': 1}
    log = med_api.upload_data(fid=fid_for_manual_upload, **data_post)

    # b)
    time.sleep(file_update_time)  # Give db time to update
    correct_filename_versions = [pytest.usecase_1_filename_version, pytest.usecase_2_filename_version]
    filename_versions = med_api.extract_var(med_api.get_files(studyid='TEST', versionid=1), 'filename_version')
    assert all([fid in filename_versions for fid in correct_filename_versions]), "Missing expected filename versions from two previous usecases"

    # c)
    time.sleep(data_update_time-file_update_time)  # Give db time to update
    data_rows = med_api.get_data(studyid='TEST', versionid=1, format='flat_dict')
    correct_row_2 = pytest.usecase_2_row2.copy()
    correct_row_2.update({'memtesta.accuracy': 0.9, 'visitid': 1})
    pytest.usecase_3_row2 = correct_row_2
    correct_rows = [pytest.usecase_2_row1, correct_row_2]
    for correct_row in correct_rows:
        assert any([dict_issubset(data_row, correct_row) for data_row in data_rows])




@pytest.mark.dependency(['test_usecase_3'])
def test_usecase_4(med_api):

    # a) see fixture

    # some hackery to get these sleep score files to parse
    remove_all_study_settings(med_api)
    study_settings_file = os.path.join(my_path,'testfiles/MednickHumeType_study_settings.yaml')
    with open(study_settings_file, 'rb') as f:
        study_settings_info = med_api.upload_file(fileobject=f, filetype='study_settings', fileformat='study_settings', studyid='TEST', versionid=1)

    time.sleep(2)

    # b) uploading some scorefiles
    file_info1_post = {
        'fileformat':'sleep_scoring',
        'studyid':'TEST',
        'versionid':1,
        'subjectid':2,
        'visitid':1,
        'sessionid':1,
        'filetype':'sleep_scoring'
    }
    with open(os.path.join(os.path.dirname(__file__),'testfiles/scorefile1.mat'), 'rb') as scorefile1:
        fid1 = med_api.upload_file(scorefile1,
                                   **file_info1_post)

    file_info2_post = file_info1_post.copy()
    file_info2_post.update({'visitid':2})
    with open(os.path.join(os.path.dirname(__file__),'testfiles/scorefile2.mat'), 'rb') as scorefile2:
        fid2 = med_api.upload_file(scorefile2,
                                   **file_info2_post)

    scorefile1_data = {'sleep_scoring.epochstages': ['wbso', 'wbso', 'wbso', 'wbso', 'wbso', 'wbso', 'wbso'],
                       'sleep_scoring.epochoffset_secs':102,
                       'sleep_scoring.start_datetime': datetime(2016, 1, 1, 0, 0), 'sleep_scoring.mins_in_waso': 0, 'sleep_scoring.mins_in_n1': 0,
                       'sleep_scoring.mins_in_n2': 0, 'sleep_scoring.mins_in_n3': 0, 'sleep_scoring.mins_in_rem': 0,
                       'sleep_scoring.sleep_efficiency': None, 'sleep_scoring.total_sleep_time_mins': 0}
    scorefile2_data = {'sleep_scoring.epochstages': ['wbso', 'wbso', 'n1', 'n1', 'n2', 'n2', 'n3', 'n3', 'n2', 'n2'],
                       'sleep_scoring.start_datetime': datetime(2016, 1, 1, 0, 0), 'sleep_scoring.mins_in_waso': 0, 'sleep_scoring.mins_in_n1': 1,
                       'sleep_scoring.mins_in_n2': 2, 'sleep_scoring.mins_in_n3': 1, 'sleep_scoring.mins_in_rem': 0,
                       'sleep_scoring.sleep_efficiency': 1.0, 'sleep_scoring.total_sleep_time_mins': 4}

    # c)
    time.sleep(data_update_time)  # Give db 50 seconds to update
    data_rows = med_api.get_data(studyid='TEST', versionid=1, format='flat_dict')
    correct_row_1 = pytest.usecase_2_row1.copy()
    scorefile1_data.update(pytest.usecase_3_row2)
    correct_row_2 = scorefile1_data
    scorefile2_data.update(pytest.usecase_2_row2)
    correct_row_3 = scorefile2_data
    correct_rows = [correct_row_1, correct_row_2, correct_row_3]
    for correct_row in correct_rows:
        assert any([dict_issubset(data_row, correct_row) for data_row in data_rows])

    pytest.usecase_4_row1 = correct_row_1
    pytest.usecase_4_row2 = correct_row_2
    pytest.usecase_4_row3 = correct_row_3

    med_api.delete_file(fid=study_settings_info['_id'], delete_all_versions=True)


@pytest.mark.dependency(['test_usecase_4'])
def test_usecase_5(med_api):
    # a)
    data_rows = med_api.get_data(query='studyid=TEST and data.memtesta.accuracy>=0.9', format='flat_dict')
    assert any([dict_issubset(data_row, pytest.usecase_3_row2) for data_row in data_rows])


def test_get_specifiers(med_api):
    sids = med_api.get_unique_var_values('studyid', store='data')
    assert 'TEST' in sids

    vids = med_api.get_unique_var_values('versionid', studyid='TEST', store='data')
    assert vids == [1]

    sids = med_api.get_unique_var_values('subjectid', studyid='TEST', store='data')
    assert sids == [1, 2]

    vids = med_api.get_unique_var_values('visitid', studyid='TEST', store='data')
    assert vids == [1, 2]

    sids = med_api.get_unique_var_values('sessionid', studyid='TEST', store='data')
    assert sids == [1]

    #filetypes = med_api.get_unique_var_values('filetype', studyid='TEST', store='data')
    #assert set(filetypes) == {'sleep_eeg', 'sleep_scoring', 'demographics', 'memtesta'}

@pytest.mark.dependency(['test_clear_test_study'])
def test_study_settings_upload(med_api):
    remove_all_study_settings(med_api)
    study_settings_file = os.path.join(my_path,'testfiles/example1_study_settings.yaml')
    with open(study_settings_file, 'rb') as f:
        pytest.study_setting_info = med_api.upload_file(fileobject=f, filetype='study_settings', fileformat='study_settings', studyid='TEST', versionid=1)

    study_settings_file_info = med_api.get_files(filetype='study_settings', studyid='TEST', versionid=1)[0]
    assert study_settings_file_info['studyid'] == 'TEST'
    assert study_settings_file_info['versionid'] == 1


@pytest.mark.dependency(['test_clear_test_study', 'test_study_settings_upload'])
def test_file_upload_that_gens_new_file(med_api):
    """Tests that a raw sleep file + scoring file can be uploaded and:
    a) Raw data variables extracted
    b) A std version of that edf file created
    c) Sleep features and band_power data and files extracted from it using scoring
    """
    file_info = {
        'studyid':'TEST',
        'versionid':1,
        'subjectid':1,
        'visitid':1,
        'sessionid':1,
    }

    study_settings_file = os.path.join(my_path,'testfiles/example1_sleep_rec.edf')
    with open(study_settings_file, 'rb') as f:
        file_info_sleep_rec = med_api.upload_file(fileobject=f, filetype='raw_sleep_eeg', fileformat='raw_sleep_eeg', **file_info)
    # This should generate a new std_sleep_eeg file


    study_settings_file = os.path.join(my_path,'testfiles/example1_epoch_stages.pkl')
    epoch_stages = pickle.load(open(study_settings_file, 'rb'))
    med_api.upload_data(data={'epochstages': epoch_stages, 'lights_off_secs':0, 'lights_on_secs':len(epoch_stages)*30}, filetype='sleep_scoring', fid=file_info_sleep_rec['_id'], **file_info)
    # This should generate a new sleep_features file, and band_power_per_epoch
    # Plus a bunch of std_sleep_eeg data

    time.sleep(15)  # wait 20 seconds while example file is parsed and uploaded

    # check all generated stuffs
    new_std_sleep_eeg_info = med_api.get_files(filetype='std_sleep_eeg', **file_info)[0]
    correct_file_info = file_info.copy()
    correct_file_info['filetype'] = 'std_sleep_eeg'
    correct_file_info['fileformat'] = 'std_sleep_eeg'
    assert all([new_std_sleep_eeg_info[k] == v for k, v in correct_file_info.items()])


    new_scoring_sleep_eeg_info = med_api.get_files(filetype='scoring_sleep_eeg', **file_info)[0]
    correct_file_info = file_info.copy()
    correct_file_info['filetype'] = 'scoring_sleep_eeg'
    correct_file_info['fileformat'] = 'scoring_sleep_eeg'
    assert all([new_scoring_sleep_eeg_info[k] == v for k, v in correct_file_info.items()])

    time.sleep(15) # wait 20 seconds while sleep features extracted, #FIXME more when rem is added

    new_sleep_features_info = med_api.get_files(filetype='sleep_features', **file_info)[0]
    correct_file_info = file_info.copy()
    correct_file_info['filetype'] = 'sleep_features'
    correct_file_info['fileformat'] = 'sleep_features'
    assert all([new_sleep_features_info[k] == v for k, v in correct_file_info.items()])

    new_band_power_per_epoch_info = med_api.get_files(filetype='band_power', **file_info)[0]
    correct_file_info = file_info.copy()
    correct_file_info['filetype'] = 'band_power'
    correct_file_info['fileformat'] = 'band_power'
    assert all([new_band_power_per_epoch_info[k] == v for k, v in correct_file_info.items()])

    std_sleep_eeg_data = med_api.get_data(filetype='std_sleep_eeg', format='flat_dict', **file_info)[0]
    assert 'sleep_scoring.epochstages' in std_sleep_eeg_data
    assert 'band_power.n1_C3_SWA' in std_sleep_eeg_data # something from freq bands
    assert 'sleep_features.n2_C3_spindle_count' in std_sleep_eeg_data #something from features

    med_api.delete_file(fid=pytest.study_setting_info['_id'], delete_all_versions=True)
