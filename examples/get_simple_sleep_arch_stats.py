"""This is an example file of how to download and plot some data from the mednickdb"""
import sys, os
my_path = os.path.dirname('')
sys.path.append(my_path+'../')
from mednickdb_pyapi.pyapi import MednickAPI
import seaborn as sns
import matplotlib.pyplot as plt
import os
import statsmodels.formula.api as smf

#initalize the API
username = 'mednickdb.testing@gmail.com' #<--- your email goes here
password = os.environ['PASSWORD'] #<--- your ps goes here, e.g. password = 'my_password'
med_api = MednickAPI(username=username, password=password)

#Lets get some data, well get all data that has some sleep scoring data:
data = med_api.get_data(query='studyid=cfs and filetype=sleep_scoring')
#There are lots of extra columns here, so lets just get keep the ones we want to analyze!
# we first select our columns of interest, then drop any missing data rows
data = data.loc[:, ['sleep_scoring.total_sleep_time','demographics.ethnicity','demographics.sex']].dropna()

if data is None: #some error checking, when a query returns None, this means no data was found...
    raise ValueError('No data found for this query!')
else:
    print('Got',data.shape[0],'records')

print(data.head()) #lets print a few rows of data

#lets have a look at ethnicity and sex effects on sleep arch:
sns.violinplot(x='demographics.sex', y='sleep_scoring.total_sleep_time', data=data)
plt.show()

#lets have a look at ethnicity and sex effects on sleep arch:
sns.violinplot(x='demographics.ethnicity', y='sleep_scoring.total_sleep_time', data=data)
plt.show()

#Its that easy :)

#if you would rather strip the filtype from the column names, its easy:
data.columns = [var_name.split('.')[1] for var_name in data.columns] #split each column at "." and keep the second half

# we can also run a regression:
#using r style syntax, this is a regression with DV=total_sleep_time, and IV's=ethnicity and sex + interaction.
mod = smf.ols(formula='total_sleep_time ~ ethnicity + sex + sex*ethnicity', data=data) #stats models will automatically code our categorical variables
res = mod.fit()

print(res.summary()) #print a nice formated summary of the model fit data :)