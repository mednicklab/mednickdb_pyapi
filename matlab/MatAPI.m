classdef MatAPI
    properties
        username
        password
        server_address
    end
    methods
        function obj = MatAPI(username, password, server_address)
            obj.username = username;
            obj.password = password;
            if ~exist('server_address','var')
                server_address = 'saclab.ss.uci.edu:8000';
            end
            obj.server_address = server_address;
        end
        
        function med_api = login(obj)
            try
                med_api = py.mednickdb_pyapi.pyapi.MednickAPI(obj.username, obj.password, obj.server_address);
            catch e
                if strcmp(e.identifier,'MATLAB:undefinedVarOrClass')
                    error('The python based pyapi is not installed correctly. See readme.') 
                end
                rethrow(e)
            end
        end
        
        function output_args = extract_args(~, args, to_add)
            if isstruct(args)
                args_cont = {};
                fnames = fieldnames(args);
                for f = 1:length(fnames)
                    args_cont = [args_cont fnames{f} args.(fnames{f})];
                end
                args = args_cont;
            end
            args = [args to_add];
            output_args = pyargs(args{:});
        end
        
        function data_table = get_data(obj, vargin)
            %format is not suported as an input argument. Table will be returned only!
            py_args = obj.extract_args(vargin, {'format', 'to_csv', 'file_name', [pwd filesep 'temp.csv']});
            med_api = obj.login();
            data_exists = med_api.get_data(py_args);
            if ~isempty(cell(data_exists))
                data_table = readtable([pwd filesep 'temp.csv']);
                delete([pwd filesep 'temp.csv'])
            else
                disp('No data for that query')
                data_table = false;
            end
        end
        
        function file_table = get_files(obj, vargin)
            %format is not suported as an input argument. Table will be returned only!
            py_args = obj.extract_args(vargin, {'format', 'to_csv', 'file_name', [pwd filesep 'temp.csv']});
            med_api = obj.login();
            data_exists = med_api.get_files(py_args);
            if ~isempty(cell(data_exists))
                file_table = readtable([pwd filesep 'temp.csv']);
                delete([pwd filesep 'temp.csv'])
            else
                disp('No files for that query')
                file_table = false;
            end
        end
    
    end
end

