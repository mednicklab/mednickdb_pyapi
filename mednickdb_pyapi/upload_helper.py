import sys
import glob
import re
from typing import List, Tuple, Union
from mednickdb_pyapi.pyapi import MednickAPI
import getpass
import inspect
import os
import yaml
import requests
import stat

re_i_type = r'\d+'
re_s_type = r'[a-z0-9]*'

default_pattern_keys = {
    'subjectid': re_i_type,
    'studyid': re_s_type,
    'visitid': re_i_type,
    'sessionid': re_i_type,
    'versionid': re_i_type,
    'filetype': re_s_type,
    'fileformat': re_s_type
}

type_map = {'int':r'\d+',
            'str':r'[A-z0-9]*'}


def _parse_args_to_reg_ex(search_pattern: str) -> Tuple[Union[None,str], List[str], List[str]]:
    """
    Take a human readable string pattern and parse to a re expression, and a set of keys and types to extract from a filename
    :param search_pattern: A string to match files to, where specifiers, like subjectid, visitid, etc can be extracted by adding {subjectid}.
        e.g. if a filename is mydata_subid43, then mydata_subid{subjectid} will create a regular expression to
        match mydata_subid and also extract 43 as the subjectid.
    :return:
    """
    # parse filename matching
    if search_pattern is None:
        return None, [], []
    pattern_to_match = search_pattern.replace('*',r'[\s\S]*') #deal with wildcards
    pattern_components = pattern_to_match.split('}')[:-1]
    end_str_to_match = pattern_to_match.split('}')[-1]
    pattern_stems = [pattern.split('{')[0] for pattern in pattern_components]
    pattern_key_parts = [pattern.split('{')[1] for pattern in pattern_components]
    pattern_keys = []
    pattern_types = []
    for pattern_key_part in pattern_key_parts:
        if '=' in pattern_key_part:
            pattern_types.append(type_map[pattern_key_part.split('=')[1]])
            pattern_keys.append(pattern_key_part.split('=')[0])
        else:
            pattern_types.append(
                default_pattern_keys[pattern_key_part] if pattern_key_part in default_pattern_keys else r'[A-z0-9]*')
            pattern_keys.append(pattern_key_part)

    re_exp = ''
    for stem, type_ in zip(pattern_stems, pattern_types):
        re_exp += stem + '(' + type_ + ')'
    re_exp += end_str_to_match

    if os.sep == '\\':
        re_exp = re_exp.replace('/', '\\\\')

    return re_exp, pattern_keys, pattern_types


def _file_path_to_upload_info(file_path: str, re_exp: str, pattern_keys: List[str], pattern_types: List[str]) -> dict:
    """
    To be used in conjunction with _parse_args_to_reg_ex.
    Parsing can automatically remove parts of the filename and set as any
    :param file_path: file path to parse.
    :param re_exp: regular expression string to match
    :param pattern_keys: names of the keys, in order for the created file_info object
    :param pattern_types: types for the values to be extracted from
    :return: upload_info, a dict of values extracted from file_path with types pattern_types, and keys pattern_keys
    """
    if os.path.isdir(file_path):
        raise IsADirectoryError(file_path+' is a dirrectory')
    values = re.search(re_exp, file_path, re.IGNORECASE)
    file_info = {}
    if values is not None:
        for pattern_key, value, dtype in zip(pattern_keys, values.groups(), pattern_types):
            if dtype == re_i_type:
                value = int(value)
            file_info[pattern_key] = value
    else:
        raise ValueError('Pattern was not matched: '+re_exp+' is not in '+file_path)
    return file_info


def run_upload_helper(folder_to_search: str, pattern: str, default_upload_args: dict=None,
                      username: str=None, password: str=None, auto_mode: bool=False, med_api=None,
                      files_to_ignore=None) -> Union[Tuple[List[dict], List[dict]], Tuple[None, None]]:
    """
    Searches through a folder and uploads file that match pattern. Can extract upload dict key:value pairs from filenames.
    :param folder_to_search: abs or regular path to folder to search through e.g. 'C:/Users/bdyet/data/'
    :param pattern: only upload files that match this pattern. Key/Valye pairs to add to upload dict can be added with {keyname},
        e.g. if the file name is eeg_subid32.edf, then subid{subjectid} will match that file, and also add subjectid=32 to the upload dict.
        types (either int or str) of the values will be inferred if known (e.g. subjectid->int, studyid->str) or can be set subid{subjectid=str}
    :param default_upload_args: common key/value pairs to add to all the matched files upload dict, as a string.
        e.g. 'studyid=PSTIM otherkey=somevalue' or dict {'studyid':'PSTIM'}
    :param username: username for mednickdb, will be requested through stdin if None
    :param password: password for mednickdb, will be requested through stdin if None
    :param auto_mode: Wether to check with the user if the uploaded files are good (True=check, False=upload regardless)
    :param med_api: An mednick API object, if none, login will be asked for
    :return: (a list of files to upload, and a list of successfully uploaded files) or (None, None) if no files uploaded
    """
    if med_api is None:
        if username is None:
            username = input('MednickDB Username: ')
        if password is None:
            password = getpass.getpass()
        med_api = MednickAPI(username=username, password=password)

    files_ready_to_upload, n_files = _gather_files_to_upload(folder_to_search, pattern, default_upload_args)
    files_to_actually_upload = []

    print('%i files in folder, %i match pattern and are ready for upload' % (n_files, len(files_ready_to_upload)))

    if auto_mode:
        print('**AUTO_MODE enabled**')
        files_to_actually_upload = files_ready_to_upload
        print('--uploading all files')
    else:
        print('Files are:')
        for idx, file_info in enumerate(files_ready_to_upload):
            print(idx, ':', file_info['filepath'])
            for key, value in file_info.items():
                if key is not 'filepath':
                    print('   ', key, '=', value)
            ans = input('Is this correct? (y-yes, n-no, Y-yes to all, N-no to remainder, q-quit/cancel)?')
            if ans == 'Y':
                files_to_actually_upload += files_ready_to_upload[idx:]
                print('--uploading all files')
                break
            if ans == 'N':
                print('--uploading no more files')
                break
            elif ans == 'y':
                files_to_actually_upload.append(file_info)
                print('--upload this file')
            elif ans == 'n':
                print('--skipped this file')
            else:
                print('Quitting, no files uploaded')
                return None, None

    if files_to_ignore is not None:
        print('Removing files in ignore list')
        before_remove = len(files_to_actually_upload)
        for file in files_to_ignore:
            if isinstance(file, str):
                matching_files = [to_upload_file for to_upload_file in files_to_actually_upload if file in to_upload_file['filepath']]
                for f in matching_files:
                    files_to_actually_upload.remove(f)
        print('Removed',before_remove-len(files_to_actually_upload),'files that were in ignore list')

    if len(files_to_actually_upload) < 1:
        print('Warning: Uploading 0 files!')
        return None, None

    files_actually_uploaded = []
    for file_idx, file in enumerate(files_to_actually_upload):
        file_ = file.copy()
        if files_to_ignore is not None:
            file_to_ignore_dicts = [f for f in files_to_ignore if isinstance(f, dict)]
            if any(all(item in file_.items() for item in file_to_ignore.items()) for file_to_ignore in file_to_ignore_dicts):
                print(file['filepath'],'in ignore list')
                continue
        with open(file_.pop('filepath'), 'rb') as uploaded_version:
            args = inspect.signature(med_api.upload_file)
            for k in file_:
                if k not in args.parameters.keys():
                    del file_[k]
            for upload_trys in range(10):
                try:
                    print('uploading file',file_idx+1, 'of', len(files_to_actually_upload))
                    file_info = med_api.upload_file(fileobject=uploaded_version,
                                                    fileformat=file_.pop('fileformat'),
                                                    filetype=file_.pop('filetype'),
                                                    studyid=file_.pop('studyid'),
                                                    versionid=file_.pop('versionid'),
                                                    **file_) #all the other stuff
                    break
                except requests.exceptions.ConnectionError:
                    print('Server Disconnected. Retrying. Uploaded', file_idx, 'files')
                    if upload_trys == 9:
                        raise

            else:
                raise ConnectionError

            files_actually_uploaded.append(file_info)

    return files_to_actually_upload, files_actually_uploaded


def _gather_files_to_upload(folder_to_search: str, pattern: str, default_upload_args: dict=None) -> Tuple[List[dict], int]:
    """
    Searches through a folder recursively and uploads file that match pattern. Can extract upload dict key:value pairs from filenames.
    :param folder_to_search: abs or regular path to folder to search through e.g. 'C:/Users/bdyet/data/'
    :param pattern: only upload files that match this pattern. Key/Valye pairs to add to upload dict can be added with {keyname},
        e.g. if the file name is eeg_subid32.edf, then subid{subjectid} will match that file, and also add subjectid=32 to the upload dict
    :param default_upload_args: common key/value pairs to add to all the matched files upload dic, as a string.
        e.g. 'studyid=PSTIM otherkey=somevalue'
    :return: a list of upload dicts for the files to upload. File path will be added as a "filename" key to each.
    """

    req_upload_keys = {'fileformat', 'filetype', 'studyid', 'versionid'}

    if folder_to_search[-1] is not '/' or folder_to_search[-1] is not '\\':
        folder_to_search += '/'

    # parse filename matching
    re_exp, pattern_keys, pattern_types = _parse_args_to_reg_ex(pattern)

    # parse specifiers for the whole folder
    if default_upload_args is not None:
        files_info = default_upload_args
        all_keys = set(pattern_keys + list(files_info.keys()))
    else:
        files_info = {}
        all_keys = set(pattern_keys)

    assert all_keys.issuperset(req_upload_keys), "Not all required specifiers given. You need at least: 'fileformat', 'filetype', 'studyid', 'versionid'"

    file_infos = []
    dont_upload_str_files = []
    hidden_files = []
    files_in_folder = glob.glob(folder_to_search + '**', recursive=True)
    num_to_upload = len(files_in_folder)
    for file_path in files_in_folder:
        if os.path.isdir(file_path):
            num_to_upload -= 1
            continue
        if 'dont_upload' in file_path:
            dont_upload_str_files.append(file_path)
            continue
        if has_hidden_attribute(file_path):
            hidden_files.append(file_path)
            continue
        if re_exp is not None:
            try:
                file_info = _file_path_to_upload_info(file_path, re_exp, pattern_keys, pattern_types)
            except (ValueError, IsADirectoryError):  #pattern not in file or found dir
                continue
            file_info.update(files_info)
        else:
            file_info = file_infos.copy()
        file_info['filepath'] = file_path
        file_infos.append(file_info)
    if len(dont_upload_str_files) > 0:
        print(len(dont_upload_str_files),'Files have "dont_upload" in their filename, and will be skipped')
    if len(hidden_files) > 0:
        print(len(hidden_files),'hidden files detected, these will be skipped')
    return file_infos, num_to_upload


def has_hidden_attribute(filepath):
    return bool(os.stat(filepath).st_file_attributes & stat.FILE_ATTRIBUTE_HIDDEN)


def extract_key_values_from_filepath(file_path: str, pattern: str) -> dict:
    """
    A helper function to extract key:value pairs from a file path.
    :param file_path: abs or regular path to search through
    :param pattern: Key/Value pairs to add to upload dict can be added with {keyname},
        e.g. if the file name is eeg_subid32.edf, then subid{subjectid} will match that file, and adds subjectid=32 to the returned dict.
        types (either int or str) of the values will be inferred if known (e.g. subjectid->int, studyid->str) or can be set subid{subjectid=str}
    :return: a dict of key:value pairs extracted from the file path
    """
    re_exp, pattern_keys, pattern_types = _parse_args_to_reg_ex(pattern)
    return _file_path_to_upload_info(file_path, re_exp, pattern_keys, pattern_types)


def upload_via_upload_spec(folder_to_search_in: str='./', username=None, password=None, auto_mode=False, filetypes_to_ignore=[], files_to_ignore=[]):
    """
    Upload files to the mednickdb using a upload_spec.yaml file.
    :param folder_to_search_in: where to find the upload spec file relative to where you called python.
    Leave out if calling from the same folder as the upload_spec.yaml
    :return:
    """
    if username is None:
        username = input('MednickDB Username: ')
    if password is None:
        password = getpass.getpass()
    med_api = MednickAPI(username=username, password=password)
    upload_spec_search = os.path.join(folder_to_search_in,'*upload_spec*.yaml')
    upload_spec_path = glob.glob(upload_spec_search)
    assert len(upload_spec_path) >= 1, 'No upload_spec.yaml found in given folder. Are you running python from the folder want to upload from?'
    assert len(upload_spec_path) == 1, 'To many upload_specs.yaml found, delete the one you dont want'
    upload_spec = yaml.safe_load(open(upload_spec_path[0],'r+'))
    files_uploaded_cont = []
    base_upload_dict = {}
    for k in list(upload_spec.keys()):
        if k in ['subjectid','studyid','versionid','fileformat','visitid','sessionid']:
            base_upload_dict[k] = upload_spec.pop(k)
    for filetype, specs in upload_spec.items():
        if filetype in filetypes_to_ignore:
            print('Ignoreing upload for',filetype,'filetype')
            continue
        print('Working on uploading', filetype)
        parse_template = specs['parse_template'] if 'parse_template' in specs else '*'
        upload_dict = {k:v for k, v in specs.items() if k in ['subjectid','studyid','versionid','fileformat','visitid','sessionid']}
        upload_dict['filetype'] = filetype
        upload_dict.update(base_upload_dict)
        if 'fileformat' not in upload_dict:
            upload_dict['fileformat'] = filetype
        search_foler = os.path.join(folder_to_search_in,os.path.join(specs['folder'], ''))
        _, file_infos_uploaded = run_upload_helper(search_foler, parse_template, upload_dict, med_api=med_api, auto_mode=auto_mode, files_to_ignore=files_to_ignore)
        if file_infos_uploaded is not None:
            files_uploaded_cont += file_infos_uploaded
    return files_uploaded_cont


if __name__ == '__main__':
    if len(sys.argv)>2:
        other_specifiers = {other_spec.split('=')[0]: other_spec.split('=')[1] for other_spec in sys.argv[3:]}
    else:
        other_specifiers = None
    run_upload_helper(sys.argv[1], sys.argv[2], other_specifiers)



