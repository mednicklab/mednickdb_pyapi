from typing import List, Tuple, Union
import pandas as pd
import itertools
import numpy as np


def wide_to_long(df: pd.DataFrame, col_format_map: dict) -> pd.DataFrame:
    """
    Convert a table in wide format to long format. Google if your not sure what this means ;)
    :param df: a dataframe of wide format data
    :param col_format_map: describes the patterns that specify factor variables (variables to make columns for)
        This should be in a dict where each column name is a key, and the value is another dict that maps the strings that specify the factors, to the value of those factors
        For example:
         #col_format_map = {'visitid': {'V1': 1, 'V2': 2, 'V3': 3}, # here we are saying visitid's are coded as V1, V2, V3 in each column, and we would like to recode them as numbers
                            'sessionid': {'PM': 'PM', # each sessionid is coded as PM or AM in the column headers, and we'd like to keep those names as our sessions
                                          'AM': 'AM'}}
    :return: wide format dataframe
    """

    for spec, stub_map in col_format_map.items():
        id_vars = [col for col in df.columns if all([stub not in col for stub in stub_map])]
        stub_names = set()
        for stub in stub_map.keys():
            stub_names |= set([col.replace(stub,'') for col in df.columns if stub in col and col not in id_vars])
            df.columns = [col.replace(stub,'')+stub if stub in col else col for col in df.columns]
        df['id'] = df.index.tolist()
        suf = '(!?'+'|'.join(stub_map.keys())+')'
        df = pd.wide_to_long(df, stubnames=stub_names, i='id', j=spec, suffix=suf)
        df = df.reset_index().drop('id', axis=1)
        df[spec] = df[spec].map(stub_map)
        print('Hs')
    for _ in col_format_map:
        df.columns = [col.lstrip('_').rstrip('_') for col in df.columns]
    spec = [s for s in ['studyid','versionid','subjectid','visitid','sessionid','filetype'] if s in df.columns]
    others = [c for c in df.columns if c not in ['studyid','versionid','subjectid','visitid','sessionid','filetype']]
    df = df.loc[:, spec+others]
    df = df.sort_values(spec)
    return df


def long_to_wide(df: pd.DataFrame,
                 one_row_per: Union[List[str],str],
                 factor_variables: Union[List[str],str],
                 variables_to_ignore: Union[List, str, None] = None,
                 value_variables: Union[List[str],str,None]=None,
                 insert_factor_name_in_col_name=True) -> pd.DataFrame:
    """
    Pivots a dataframe in long format to wide format
    :param df: the dataframe to pivot
    :param one_row_per: what variables (or combination of variables) that you'd like a single row for. Probably 'subjectid'
    :param variables_to_ignore: variables not to pivot (variables whoes values are repeated)
    :param factor_variables: what variables are factors: every unique combination will be added as new columns
    :param value_variables: what values contain the actual (number) values you would like in these new columns.
        If None, this will be all remaining columns
    :param insert_factor_name_in_col_name: if the factor name should be included in the new column,
    e.g. if factors are visitid=[1,2], then new columns will be value1_visitid1, if false then value1_1
    :return: the wide format df
    """
    if isinstance(one_row_per, str):
        one_row_per = [one_row_per]

    if isinstance(factor_variables, str):
        factor_variables = [factor_variables]

    if isinstance(variables_to_ignore, str):
        variables_to_ignore = [variables_to_ignore]

    if variables_to_ignore is not None:
        one_row_per += variables_to_ignore

    if value_variables is None:
        value_variables = [col for col in df.columns if col not in factor_variables+one_row_per]
    else:
        if isinstance(value_variables, str):
            value_variables = [value_variables]

    pivoted_table = df.pivot_table(index=one_row_per, columns=factor_variables, values=value_variables, aggfunc=np.nanmean)
    if insert_factor_name_in_col_name:
        new_cols = []
        for vals in pivoted_table.columns.values:
            vals_cont = []
            for name_i, val in enumerate(vals):
                if isinstance(val, int):
                    val = str(val+1)
                name = pivoted_table.columns.names[name_i] if pivoted_table.columns.names[name_i] is not None else ''
                vals_cont.append(name + '' + val)
            new_cols.append('_'.join(vals_cont))
        pivoted_table.columns = new_cols
    else:
        pivoted_table.columns = ["_".join(v) for v in df.columns.values]

    return pivoted_table.dropna(how='all', axis=1).reset_index()