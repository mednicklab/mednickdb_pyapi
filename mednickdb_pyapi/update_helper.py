import sys
import glob
from mednickdb_pyapi.pyapi import MednickAPI
import getpass
import os

def run_update_helper(username: str=None, password: str=None, med_api=None):

    print('What would you like to do?')
    print('A) Delete files')
    print('B) Delete files and remove file data from /data/')
    print('C) Trigger re-parse of files')
    ans = input('Select A, B or C, or q to quit:')
    if ans == 'q':
        return

    if med_api is None:
        if username is None:
            username = input('MednickDB Username: ')
        if password is None:
            password = getpass.getpass()
        med_api = MednickAPI(username=username, password=password)


    query = input('Enter query to filter files on:')

    files = med_api.get_files(query=query)

    if len(files) == 0:
        print('Found no files for that query. Exiting.')
        return

    print('Found', len(files),'files')
    list_them = input('List them? y or n:')
    if list_them=='y':
        for file in files:
            print(file['filebase'])

    continue_update = input('Continue? y or n:')

    if continue_update == 'n':
        return

    print('performing operation')

    ret = []
    if ans=='A':
        for file in files:
            ret.append(med_api.delete_file(fid=file['_id'],delete_all_versions=True, remove_associated_data=True))

    if ans=='B':
        sudo_password = getpass.getpass('Enter sudo password to delete from /data/:')
        for file in files:
            ret.append(med_api.delete_file(fid=file['_id'],delete_all_versions=True, remove_associated_data=True))
            files_that_match = glob.glob('/data/mednick_server/'
                      +os.path.splitext(file['filebase'])[0]
                      +'*'+os.path.splitext(file['filebase'])[1])
            for f in files_that_match:
                print('Purging file data for', f)
                command = 'rm '+f
                os.system('echo %s|sudo -S %s' % (sudo_password, command))

    if ans=='C':
        for file in files:
            ret.append(med_api.update_parsed_status(fid=file['_id'], status=False))

    return files


if __name__ == "__main__":
    run_update_helper()