FROM python:3.6.8-slim
WORKDIR /pyapi
RUN apt-get -qq update && apt-get -qq install -y 
copy . .
RUN pip install -r requirements.txt
ARG PASSWORD
ARG USERNAME
ARG API_PORT
ARG SERVER_ADRS
ENV MEDNICKDB_TESTING_PW $PASSWORD
ENV MEDNICKDB_TESTING_EMAIL $USERNAME
ENV MEDNICKDB_API_PORT_NUMBER $API_PORT
ENV MEDNICKDB_SERVER_ADDRESS $SERVER_ADRS
CMD ["pytest","-v","tests/test_mednickdb_pyapi.py"]


